use thiserror::Error;
use url::ParseError;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Entry could not be found via on pokeapi")]
    NotFound,
    #[error("A network error has occurred: {0}")]
    NetworkError(reqwest::Error),
    #[error("Failed to map the received data to the struct. Struct needs to be changed")]
    SerializationError,
    #[error("{0}")]
    BadUrl(url::ParseError),
}

impl From<reqwest::Error> for Error {
    fn from(e: reqwest::Error) -> Self {
        Error::NetworkError(e)
    }
}

impl From<url::ParseError> for Error {
    fn from(e: ParseError) -> Self {
        Error::BadUrl(e)
    }
}
