use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::locations::location_areas::LocationArea;
use crate::locations::regions::Region;
use crate::types::Id;
use crate::utility::common_models::{GenerationGameIndex, Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#locations)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Location {
    pub id: Id<Location>,
    pub name: String,
    pub region: Option<NamedAPIResource<Region>>,
    pub names: Vec<Name>,
    pub game_indices: Vec<GenerationGameIndex>,
    pub areas: Vec<NamedAPIResource<LocationArea>>,
}

#[async_trait]
impl LoadUrl for Location {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.location().await?.url(url, client).await
    }
}
