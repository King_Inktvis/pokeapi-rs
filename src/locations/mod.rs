//! [PokéAPI docs](https://pokeapi.co/docs/v2#locations-section)
pub mod location_areas;
pub mod locations;
pub mod pal_park_areas;
pub mod regions;
