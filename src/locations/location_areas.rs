use crate::encounters::encounter_methods::EncounterMethod;
use crate::error::Error;
use crate::games::version::Version;
use crate::group_containers::LoadUrl;
use crate::locations::locations::Location;
use crate::pokemon::pokemon::Pokemon;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource, VersionEncounterDetail};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#location-areas)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct LocationArea {
    pub id: Id<LocationArea>,
    pub name: String,
    pub game_index: u8,
    pub encounter_method_rates: Vec<EncounterMethodRate>,
    pub location: NamedAPIResource<Location>,
    pub names: Vec<Name>,
    pub pokemon_encounters: Vec<PokemonEncounter>,
}

#[async_trait]
impl LoadUrl for LocationArea {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.location_area().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct EncounterMethodRate {
    pub encounter_method: NamedAPIResource<EncounterMethod>,
    pub version_details: Vec<EncounterVersionDetails>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct EncounterVersionDetails {
    pub rate: i32,
    pub version: NamedAPIResource<Version>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonEncounter {
    pub pokemon: NamedAPIResource<Pokemon>,
    pub version_details: Vec<VersionEncounterDetail>,
}
