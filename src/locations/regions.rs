use crate::error::Error;
use crate::games::generations::Generation;
use crate::games::pokedexes::Pokedex;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::LoadUrl;
use crate::locations::locations::Location;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#regions)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Region {
    pub id: Id<Region>,
    pub locations: Vec<NamedAPIResource<Location>>,
    pub name: String,
    pub names: Vec<Name>,
    pub main_generation: Option<NamedAPIResource<Generation>>,
    pub pokedexes: Vec<NamedAPIResource<Pokedex>>,
    pub version_groups: Vec<NamedAPIResource<VersionGroup>>,
}

#[async_trait]
impl LoadUrl for Region {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.region().await?.url(url, client).await
    }
}
