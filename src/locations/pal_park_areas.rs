use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use macros::Getters;
use serde::Deserialize;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#pal-park-areas)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PalParkArea {
    pub id: Id<PalParkArea>,
    pub name: String,
    pub names: Vec<Name>,
    pub pokemon_encounters: Vec<PalParkEncounterSpecies>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PalParkEncounterSpecies {
    pub base_score: u8,
    pub rate: u8,
    pub pokemon_species: NamedAPIResource<PokemonSpecies>,
}
