use crate::error::Error;
use crate::error::Error::NotFound;
use crate::Client;
use serde::Deserialize;
use std::sync::Arc;
use tokio::sync::RwLock;

#[derive(Debug)]
pub(crate) struct Store<T> {
    pub value: RwLock<Container<T>>,
}

#[derive(Debug)]
pub(crate) struct Container<T> {
    pub value: Option<Arc<ItemList<T>>>,
}

impl<T> Container<T> {
    fn new() -> Container<T> {
        Container { value: None }
    }
}

impl<T> Store<T> {
    pub fn new() -> Store<T> {
        Store {
            value: RwLock::new(Container::new()),
        }
    }
}

#[derive(Deserialize, Debug)]
pub(crate) struct ItemListJson {
    count: i32,
    results: Vec<ListResultJson>,
}

#[derive(Deserialize, Debug)]
pub(crate) struct ListResultJson {
    name: Option<String>,
    url: String,
}

#[derive(Debug)]
pub struct ItemList<T> {
    pub count: i32,
    pub results: Vec<ListResult<T>>,
}

impl<T> ItemList<T>
where
    for<'de> T: Deserialize<'de>,
{
    pub async fn name(&self, name: &str, client: &Client) -> Result<Arc<T>, Error> {
        let res = self.results.iter().find(|v| {
            if let Some(compare) = &v.name {
                compare == name
            } else {
                false
            }
        });
        match res {
            None => Err(Error::NotFound),
            Some(entry) => entry.get_item(client).await,
        }
    }

    pub async fn id(&self, id: i32, client: &Client) -> Result<Arc<T>, Error> {
        let id_text = format!("/{}/", id);
        let res = self.results.iter().find(|i| i.url.contains(&id_text));
        match res {
            Some(item) => item.get_item(client).await,
            None => Err(NotFound),
        }
    }

    pub async fn url(&self, url: &str, client: &Client) -> Result<Arc<T>, Error> {
        let res = self.results.iter().find(|v| v.url == url);
        match res {
            None => Err(Error::NotFound),
            Some(entry) => entry.get_item(client).await,
        }
    }

    pub async fn populate_cache(&self, client: &Client) {
        for chunk in self.results.chunks(client.load_chunk_size) {
            let futures: Vec<_> = chunk.iter().map(|r| r.get_item(client)).collect();
            futures::future::join_all(futures).await;
        }
    }
}

#[derive(Debug)]
pub struct ListResult<T> {
    pub name: Option<String>,
    pub url: String,
    item: RwLock<Item<T>>,
}

impl<T> ListResult<T> {
    pub async fn get_item(&self, client: &Client) -> Result<Arc<T>, Error>
    where
        for<'de> T: Deserialize<'de>,
    {
        {
            let value = self.item.read().await;
            if let Some(value) = &value.value {
                return Ok(value.clone());
            }
        }
        {
            let mut value = self.item.write().await;
            let request = client.client.get(&self.url).send().await?;
            let pokemon = request.json::<T>().await?;
            let result = Arc::new(pokemon);
            let to_insert = Some(result.clone());
            value.value = to_insert;
            Ok(result)
        }
    }
}

#[derive(Debug)]
pub(crate) struct Item<T> {
    value: Option<Arc<T>>,
}

impl<T> From<ItemListJson> for ItemList<T> {
    fn from(json: ItemListJson) -> Self {
        ItemList {
            count: json.count,
            results: json.results.into_iter().map(ListResult::from).collect(),
        }
    }
}

impl<T> From<ListResultJson> for ListResult<T> {
    fn from(json: ListResultJson) -> Self {
        ListResult {
            name: json.name,
            url: json.url,
            item: Default::default(),
        }
    }
}

impl<T> std::default::Default for Item<T> {
    fn default() -> Self {
        Item { value: None }
    }
}

use async_trait::async_trait;

#[async_trait]
pub trait LoadUrl {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error>;
}
