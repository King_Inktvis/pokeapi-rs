use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::items::item::Item;
use crate::types::Id;
use crate::utility::common_models::{Description, Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#item-attributes)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ItemAttribute {
    pub id: Id<ItemAttribute>,
    pub name: String,
    pub items: Vec<NamedAPIResource<Item>>,
    pub names: Vec<Name>,
    pub descriptions: Vec<Description>,
}

#[async_trait]
impl LoadUrl for ItemAttribute {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.item_attribute().await?.url(url, client).await
    }
}
