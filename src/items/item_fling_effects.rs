use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::items::item::Item;
use crate::types::Id;
use crate::utility::common_models::{Effect, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#item-fling-effects)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ItemFlingEffect {
    pub id: Id<ItemFlingEffect>,
    pub name: String,
    pub effect_entries: Vec<Effect>,
    pub items: Vec<NamedAPIResource<Item>>,
}

#[async_trait]
impl LoadUrl for ItemFlingEffect {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.item_fling_effect().await?.url(url, client).await
    }
}
