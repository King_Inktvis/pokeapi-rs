use crate::error::Error;
use crate::evolution::evolution_chains::EvolutionChain;
use crate::games::version::Version;
use crate::group_containers::LoadUrl;
use crate::items::item_attributes::ItemAttribute;
use crate::items::item_categories::ItemCategory;
use crate::items::item_fling_effects::ItemFlingEffect;
use crate::pokemon::pokemon::Pokemon;
use crate::types::Id;
use crate::utility::common_models::{
    APIResource, GenerationGameIndex, MachineVersionDetail, Name, NamedAPIResource, VerboseEffect,
    VersionGroupFlavorText,
};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#item)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Item {
    pub id: Id<Item>,
    pub name: String,
    pub cost: i32,
    pub fling_power: Option<u8>,
    pub fling_effect: Option<NamedAPIResource<ItemFlingEffect>>,
    pub attributes: Vec<NamedAPIResource<ItemAttribute>>,
    pub category: NamedAPIResource<ItemCategory>,
    pub effect_entries: Vec<VerboseEffect>,
    pub flavor_text_entries: Vec<VersionGroupFlavorText>,
    pub game_indices: Vec<GenerationGameIndex>,
    pub names: Vec<Name>,
    pub sprites: ItemSprites,
    pub held_by_pokemon: Vec<ItemHolderPokemon>,
    pub baby_trigger_for: Option<APIResource<EvolutionChain>>,
    pub machines: Vec<MachineVersionDetail>,
}

#[async_trait]
impl LoadUrl for Item {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.item().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ItemSprites {
    pub default: Option<String>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ItemHolderPokemon {
    pub pokemon: NamedAPIResource<Pokemon>,
    pub version_details: Vec<ItemHolderPokemonVersionDetail>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ItemHolderPokemonVersionDetail {
    pub rarity: u8,
    pub version: NamedAPIResource<Version>,
}
