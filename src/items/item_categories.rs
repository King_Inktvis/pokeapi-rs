use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::items::item::Item;
use crate::items::item_pockets::ItemPocket;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#item-categories)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ItemCategory {
    pub id: Id<ItemCategory>,
    pub name: String,
    pub items: Vec<NamedAPIResource<Item>>,
    pub names: Vec<Name>,
    pub pocket: NamedAPIResource<ItemPocket>,
}

#[async_trait]
impl LoadUrl for ItemCategory {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.item_category().await?.url(url, client).await
    }
}
