//! [PokéAPI docs](https://pokeapi.co/docs/v2#items-section)
pub mod item;
pub mod item_attributes;
pub mod item_categories;
pub mod item_fling_effects;
pub mod item_pockets;
