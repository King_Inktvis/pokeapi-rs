use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::items::item_categories::ItemCategory;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#item-pockets)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ItemPocket {
    pub id: Id<ItemPocket>,
    pub name: String,
    pub categories: Vec<NamedAPIResource<ItemCategory>>,
    pub names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for ItemPocket {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.item_pocket().await?.url(url, client).await
    }
}
