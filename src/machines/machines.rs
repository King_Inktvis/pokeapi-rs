use crate::error::Error;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::LoadUrl;
use crate::items::item::Item;
use crate::moves::moves::Move;
use crate::types::Id;
use crate::utility::common_models::NamedAPIResource;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#machines)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Machine {
    pub id: Id<Machine>,
    pub item: NamedAPIResource<Item>,
    pub r#move: NamedAPIResource<Move>,
    pub version_group: NamedAPIResource<VersionGroup>,
}

#[async_trait]
impl LoadUrl for Machine {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.machine().await?.url(url, client).await
    }
}
