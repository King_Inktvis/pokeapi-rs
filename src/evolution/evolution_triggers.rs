use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#evolution-triggers)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct EvolutionTrigger {
    pub id: Id<EvolutionTrigger>,
    pub name: String,
    pub names: Vec<Name>,
    pub pokemon_species: Vec<NamedAPIResource<PokemonSpecies>>,
}

use crate::types::Id;
use async_trait::async_trait;

#[async_trait]
impl LoadUrl for EvolutionTrigger {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.evolution_trigger().await?.url(url, client).await
    }
}
