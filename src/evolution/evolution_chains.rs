use crate::error::Error;
use crate::evolution::evolution_triggers::EvolutionTrigger;
use crate::group_containers::LoadUrl;
use crate::items::item::Item;
use crate::locations::locations::Location;
use crate::moves::moves::Move;
use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::pokemon::types::Type;
use crate::types::Id;
use crate::utility::common_models::NamedAPIResource;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#evolution-chains)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct EvolutionChain {
    pub id: Id<EvolutionChain>,
    pub baby_trigger_item: Option<NamedAPIResource<Item>>,
    pub chain: ChainLink,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ChainLink {
    pub is_baby: bool,
    pub species: NamedAPIResource<PokemonSpecies>,
    pub evolution_details: Vec<EvolutionDetail>,
    pub evolves_to: Vec<ChainLink>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct EvolutionDetail {
    pub item: Option<NamedAPIResource<Item>>,
    pub trigger: NamedAPIResource<EvolutionTrigger>,
    pub gender: Option<u8>,
    pub held_item: Option<NamedAPIResource<Item>>,
    pub known_move: Option<NamedAPIResource<Move>>,
    pub known_move_type: Option<NamedAPIResource<Type>>,
    pub location: Option<NamedAPIResource<Location>>,
    pub min_level: Option<u8>,
    pub min_happiness: Option<u8>,
    pub min_beauty: Option<u8>,
    pub min_affection: Option<u8>,
    pub needs_overworld_rain: bool,
    pub party_species: Option<NamedAPIResource<PokemonSpecies>>,
    pub party_type: Option<NamedAPIResource<Type>>,
    pub relative_physical_stats: Option<i8>,
    pub time_of_day: String,
    pub trade_species: Option<NamedAPIResource<PokemonSpecies>>,
    pub turn_upside_down: bool,
}

#[async_trait]
impl LoadUrl for EvolutionChain {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.evolution_chain().await?.url(url, client).await
    }
}
