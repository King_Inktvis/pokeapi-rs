//! [PokéAPI docs](https://pokeapi.co/docs/v2#evolution-section)
pub mod evolution_chains;
pub mod evolution_triggers;
