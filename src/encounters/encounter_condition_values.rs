use crate::encounters::encounter_conditions::EncounterCondition;
use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#encounter-condition-values)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct EncounterConditionValue {
    pub id: Id<EncounterConditionValue>,
    pub name: String,
    pub condition: NamedAPIResource<EncounterCondition>,
    pub names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for EncounterConditionValue {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client
            .encounter_condition_value()
            .await?
            .url(url, client)
            .await
    }
}
