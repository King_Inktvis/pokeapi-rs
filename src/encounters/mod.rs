//! [PokéAPI docs](https://pokeapi.co/docs/v2#encounters-section)
pub mod encounter_condition_values;
pub mod encounter_conditions;
pub mod encounter_methods;
