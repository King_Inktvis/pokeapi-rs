use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::utility::common_models::Name;
use crate::Client;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#encounter-methods)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct EncounterMethod {
    pub id: Id<EncounterMethod>,
    pub name: String,
    pub order: u8,
    pub names: Vec<Name>,
}

use crate::types::Id;
use async_trait::async_trait;

#[async_trait]
impl LoadUrl for EncounterMethod {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.encounter_method().await?.url(url, client).await
    }
}
