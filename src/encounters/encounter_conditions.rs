use crate::encounters::encounter_condition_values::EncounterConditionValue;
use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#encounter-conditions)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct EncounterCondition {
    pub id: Id<EncounterCondition>,
    pub name: String,
    pub names: Vec<Name>,
    pub values: Vec<NamedAPIResource<EncounterConditionValue>>,
}

#[async_trait]
impl LoadUrl for EncounterCondition {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.encounter_condition().await?.url(url, client).await
    }
}
