use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::types::Id;
use crate::utility::common_models::Name;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#languages)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Language {
    pub id: Id<Language>,
    pub name: String,
    pub official: bool,
    pub iso639: String,
    pub iso3166: String,
    pub names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for Language {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.language().await?.url(url, client).await
    }
}
