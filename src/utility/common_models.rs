//! [PokéAPI docs](https://pokeapi.co/docs/v2#common-models)
use crate::encounters::encounter_condition_values::EncounterConditionValue;
use crate::encounters::encounter_methods::EncounterMethod;
use crate::error::Error;
use crate::games::generations::Generation;
use crate::games::version::Version;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::LoadUrl;
use crate::machines::machines::Machine;
use crate::utility::languages::Language;
use crate::Client;
use macros::Getters;
use serde::Deserialize;
use std::marker::PhantomData;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#apiresource)
#[derive(Debug, Deserialize)]
pub struct APIResource<T>
where
    T: LoadUrl,
{
    pub url: String,
    #[serde(skip)]
    phantom: PhantomData<T>,
}

impl<T> APIResource<T>
where
    T: LoadUrl,
{
    pub async fn get(&self, client: &Client) -> Result<Arc<T>, Error> {
        T::load_url(&self.url, client).await
    }

    pub fn url(&self) -> &str {
        self.url.as_str()
    }
}

impl<T> Clone for APIResource<T>
where
    T: LoadUrl,
{
    fn clone(&self) -> Self {
        APIResource {
            url: self.url.clone(),
            phantom: Default::default(),
        }
    }
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#description)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Description {
    pub description: String,
    pub language: NamedAPIResource<Language>,
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#effect)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Effect {
    pub effect: String,
    pub language: NamedAPIResource<Language>,
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#encounter)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Encounter {
    pub min_level: u8,
    pub max_level: u8,
    pub condition_values: Vec<NamedAPIResource<EncounterConditionValue>>,
    pub chance: u8,
    pub method: NamedAPIResource<EncounterMethod>,
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#flavortext)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct FlavorText {
    pub flavor_text: String,
    pub language: NamedAPIResource<Language>,
    pub version: Option<NamedAPIResource<Version>>,
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#generationgameindex)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct GenerationGameIndex {
    pub game_index: u16,
    pub generation: NamedAPIResource<Generation>,
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#machineversiondetail)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MachineVersionDetail {
    pub machine: APIResource<Machine>,
    pub version_group: NamedAPIResource<VersionGroup>,
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#name)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Name {
    pub name: String,
    pub language: NamedAPIResource<Language>,
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#namedapiresource)
#[derive(Debug, Deserialize)]
pub struct NamedAPIResource<T>
where
    T: LoadUrl,
{
    pub name: String,
    pub url: String,
    #[serde(skip)]
    phantom: PhantomData<T>,
}

impl<T> NamedAPIResource<T>
where
    T: LoadUrl,
{
    pub async fn get(&self, client: &Client) -> Result<Arc<T>, Error> {
        T::load_url(&self.url, client).await
    }

    pub fn name(&self) -> &str {
        self.name.as_str()
    }

    pub fn url(&self) -> &str {
        self.url.as_str()
    }
}

impl<T> Clone for NamedAPIResource<T>
where
    T: LoadUrl,
{
    fn clone(&self) -> Self {
        NamedAPIResource {
            name: self.name.clone(),
            url: self.name.clone(),
            phantom: Default::default(),
        }
    }
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#verboseeffect)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct VerboseEffect {
    pub effect: String,
    pub short_effect: String,
    pub language: NamedAPIResource<Language>,
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#versionencounterdetail)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct VersionEncounterDetail {
    pub version: NamedAPIResource<Version>,
    pub max_chance: u16,
    pub encounter_details: Vec<Encounter>,
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#versiongameindex)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct VersionGameIndex {
    pub game_index: u16,
    pub version: NamedAPIResource<Version>,
}

/// [PokéAPI docs](https://pokeapi.co/docs/v2#versiongroupflavortext)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct VersionGroupFlavorText {
    pub text: String,
    pub language: NamedAPIResource<Language>,
    pub version_group: NamedAPIResource<VersionGroup>,
}
