//! [PokéAPI docs](https://pokeapi.co/docs/v2#utility-section)
pub mod common_models;
pub mod languages;
