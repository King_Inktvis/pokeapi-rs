use crate::error::Error;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::LoadUrl;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#version)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Version {
    pub id: Id<Version>,
    pub name: String,
    pub names: Vec<Name>,
    pub version_group: NamedAPIResource<VersionGroup>,
}

#[async_trait]
impl LoadUrl for Version {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.version().await?.url(url, client).await
    }
}
