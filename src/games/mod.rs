//! [PokéAPI docs](https://pokeapi.co/docs/v2#games-section)
pub mod generations;
pub mod pokedexes;
pub mod version;
pub mod version_groups;
