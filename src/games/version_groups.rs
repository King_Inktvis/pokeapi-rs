use crate::error::Error;
use crate::games::generations::Generation;
use crate::games::pokedexes::Pokedex;
use crate::games::version::Version;
use crate::group_containers::LoadUrl;
use crate::locations::regions::Region;
use crate::moves::move_learn_methods::MoveLearnMethod;
use crate::types::Id;
use crate::utility::common_models::NamedAPIResource;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#version-groups)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct VersionGroup {
    pub id: Id<VersionGroup>,
    pub name: String,
    pub order: u8,
    pub generation: NamedAPIResource<Generation>,
    pub move_learn_methods: Vec<NamedAPIResource<MoveLearnMethod>>,
    pub pokedexes: Vec<NamedAPIResource<Pokedex>>,
    pub regions: Vec<NamedAPIResource<Region>>,
    pub versions: Vec<NamedAPIResource<Version>>,
}

#[async_trait]
impl LoadUrl for VersionGroup {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.version_group().await?.url(url, client).await
    }
}
