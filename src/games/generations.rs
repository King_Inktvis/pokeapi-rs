use crate::error::Error;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::LoadUrl;
use crate::locations::regions::Region;
use crate::moves::moves::Move;
use crate::pokemon::abilities::Ability;
use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::pokemon::types::Type;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#generations)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Generation {
    pub id: Id<Generation>,
    pub name: String,
    pub abilities: Vec<NamedAPIResource<Ability>>,
    pub main_region: NamedAPIResource<Region>,
    pub moves: Vec<NamedAPIResource<Move>>,
    pub names: Vec<Name>,
    pub pokemon_species: Vec<NamedAPIResource<PokemonSpecies>>,
    pub types: Vec<NamedAPIResource<Type>>,
    pub version_groups: Vec<NamedAPIResource<VersionGroup>>,
}

#[async_trait]
impl LoadUrl for Generation {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.generation().await?.url(url, client).await
    }
}
