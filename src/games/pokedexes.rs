use crate::error::Error;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::LoadUrl;
use crate::locations::regions::Region;
use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::utility::common_models::{Description, Name, NamedAPIResource};
use crate::Client;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#pokedexes)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Pokedex {
    pub id: Id<Pokedex>,
    pub name: String,
    pub is_main_series: bool,
    pub descriptions: Vec<Description>,
    pub names: Vec<Name>,
    pub pokemon_entries: Vec<PokemonEntry>,
    pub region: Option<NamedAPIResource<Region>>,
    pub version_groups: Vec<NamedAPIResource<VersionGroup>>,
}

use crate::types::Id;
use async_trait::async_trait;

#[async_trait]
impl LoadUrl for Pokedex {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.pokedex().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonEntry {
    pub entry_number: u16,
    pub pokemon_species: NamedAPIResource<PokemonSpecies>,
}
