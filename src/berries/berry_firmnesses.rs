use crate::berries::berries::Berry;
use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#berry-firmnesses)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct BerryFirmness {
    pub id: Id<BerryFirmness>,
    pub name: String,
    pub berries: Vec<NamedAPIResource<Berry>>,
    pub names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for BerryFirmness {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.berry_firmness().await?.url(url, client).await
    }
}
