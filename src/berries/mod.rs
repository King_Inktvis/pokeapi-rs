//! [PokéAPI docs](https://pokeapi.co/docs/v2#berries-section)
pub mod berries;
pub mod berry_firmnesses;
pub mod berry_flavors;
