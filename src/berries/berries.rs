use crate::berries::berry_firmnesses::BerryFirmness;
use crate::berries::berry_flavors::BerryFlavor;
use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::items::item::Item;
use crate::pokemon::types::Type;
use crate::types::Id;
use crate::utility::common_models::NamedAPIResource;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#berries)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Berry {
    pub id: Id<Berry>,
    pub name: String,
    pub growth_time: u8,
    pub max_harvest: u8,
    pub natural_gift_power: u8,
    pub size: u16,
    pub smoothness: u8,
    pub soil_dryness: u8,
    pub firmness: NamedAPIResource<BerryFirmness>,
    pub flavors: Vec<BerryFlavorMap>,
    pub item: NamedAPIResource<Item>,
    pub natural_gift_type: NamedAPIResource<Type>,
}

#[async_trait]
impl LoadUrl for Berry {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.berry().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct BerryFlavorMap {
    potency: i32,
    flavor: NamedAPIResource<BerryFlavor>,
}
