use crate::berries::berries::Berry;
use crate::contests::contest_types::ContestType;
use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#berry-flavors)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct BerryFlavor {
    pub id: Id<BerryFlavor>,
    pub name: String,
    pub berries: Vec<FlavorBerryMap>,
    pub contest_type: NamedAPIResource<ContestType>,
    pub names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for BerryFlavor {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.berry_flavor().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct FlavorBerryMap {
    potency: u8,
    berry: NamedAPIResource<Berry>,
}
