use crate::berries::berry_flavors::BerryFlavor;
use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::types::Id;
use crate::utility::common_models::NamedAPIResource;
use crate::utility::languages::Language;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#contest-types)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ContestType {
    pub id: Id<ContestType>,
    pub name: String,
    pub berry_flavor: NamedAPIResource<BerryFlavor>,
    pub names: Vec<ContestName>,
}

#[async_trait]
impl LoadUrl for ContestType {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.contest_type().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ContestName {
    name: String,
    color: String,
    language: NamedAPIResource<Language>,
}
