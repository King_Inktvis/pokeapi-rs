use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::moves::moves::Move;
use crate::types::Id;
use crate::utility::common_models::{FlavorText, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#super-contest-effects)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct SuperContestEffect {
    pub id: Id<SuperContestEffect>,
    pub appeal: u8,
    pub flavor_text_entries: Vec<FlavorText>,
    pub moves: Vec<NamedAPIResource<Move>>,
}

#[async_trait]
impl LoadUrl for SuperContestEffect {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.super_contest_effect().await?.url(url, client).await
    }
}
