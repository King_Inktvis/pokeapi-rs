//! [PokéAPI docs](https://pokeapi.co/docs/v2#contests-section)
pub mod contest_effects;
pub mod contest_types;
pub mod super_contest_effects;
