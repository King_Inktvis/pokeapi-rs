use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::types::Id;
use crate::utility::common_models::{Effect, FlavorText};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#contest-effects)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ContestEffect {
    pub id: Id<ContestEffect>,
    pub appeal: u8,
    pub jam: u8,
    pub effect_entries: Vec<Effect>,
    pub flavor_text_entries: Vec<FlavorText>,
}

#[async_trait]
impl LoadUrl for ContestEffect {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.contest_effect().await?.url(url, client).await
    }
}
