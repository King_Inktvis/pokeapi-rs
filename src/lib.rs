#![recursion_limit = "256"]
#![allow(clippy::module_inception)]
use crate::berries::berries::Berry;
use crate::berries::berry_firmnesses::BerryFirmness;
use crate::berries::berry_flavors::BerryFlavor;
use crate::contests::contest_effects::ContestEffect;
use crate::contests::contest_types::ContestType;
use crate::contests::super_contest_effects::SuperContestEffect;
use crate::encounters::encounter_condition_values::EncounterConditionValue;
use crate::encounters::encounter_conditions::EncounterCondition;
use crate::encounters::encounter_methods::EncounterMethod;
use crate::error::Error;
use crate::evolution::evolution_chains::EvolutionChain;
use crate::evolution::evolution_triggers::EvolutionTrigger;
use crate::games::generations::Generation;
use crate::games::pokedexes::Pokedex;
use crate::games::version::Version;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::{ItemList, ItemListJson, Store};
use crate::items::item::Item;
use crate::items::item_attributes::ItemAttribute;
use crate::items::item_categories::ItemCategory;
use crate::items::item_fling_effects::ItemFlingEffect;
use crate::items::item_pockets::ItemPocket;
use crate::locations::location_areas::LocationArea;
use crate::locations::locations::Location;
use crate::locations::pal_park_areas::PalParkArea;
use crate::locations::regions::Region;
use crate::machines::machines::Machine;
use crate::moves::move_ailments::MoveAilment;
use crate::moves::move_battle_styles::MoveBattleStyle;
use crate::moves::move_categories::ModelName;
use crate::moves::move_damage_classes::MoveDamageClass;
use crate::moves::move_learn_methods::MoveLearnMethod;
use crate::moves::move_targets::MoveTarget;
use crate::moves::moves::Move;
use crate::pokemon::abilities::Ability;
use crate::pokemon::characteristics::Characteristic;
use crate::pokemon::egg_groups::EggGroup;
use crate::pokemon::genders::Gender;
use crate::pokemon::growth_rates::GrowthRate;
use crate::pokemon::natures::Nature;
use crate::pokemon::pokeathlon_stats::PokeathlonStat;
use crate::pokemon::pokemon::Pokemon;
use crate::pokemon::pokemon_colors::PokemonColor;
use crate::pokemon::pokemon_forms::PokemonForm;
use crate::pokemon::pokemon_habitats::PokemonHabitat;
use crate::pokemon::pokemon_shapes::PokemonShape;
use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::pokemon::stats::Stat;
use crate::pokemon::types::Type;
use crate::utility::languages::Language;
use std::sync::Arc;
use url::Url;

pub mod berries;
pub mod contests;
pub mod encounters;
pub mod error;
pub mod evolution;
pub mod games;
pub mod group_containers;
pub mod items;
pub mod locations;
pub mod machines;
pub mod moves;
pub mod pokemon;
pub mod types;
pub mod utility;

#[derive(Debug)]
pub struct Client {
    client: reqwest::Client,
    url: Url,
    load_chunk_size: usize,
    ability: Store<Ability>,
    berry: Store<Berry>,
    berry_firmness: Store<BerryFirmness>,
    berry_flavor: Store<BerryFlavor>,
    characteristic: Store<Characteristic>,
    contest_effect: Store<ContestEffect>,
    contest_type: Store<ContestType>,
    egg_group: Store<EggGroup>,
    encounter_condition: Store<EncounterCondition>,
    encounter_condition_value: Store<EncounterConditionValue>,
    encounter_method: Store<EncounterMethod>,
    evolution_chain: Store<EvolutionChain>,
    evolution_trigger: Store<EvolutionTrigger>,
    gender: Store<Gender>,
    generation: Store<Generation>,
    growth_rate: Store<GrowthRate>,
    item: Store<Item>,
    item_attribute: Store<ItemAttribute>,
    item_category: Store<ItemCategory>,
    item_fling_effect: Store<ItemFlingEffect>,
    item_pocket: Store<ItemPocket>,
    language: Store<Language>,
    location: Store<Location>,
    location_area: Store<LocationArea>,
    machine: Store<Machine>,
    r#move: Store<Move>,
    move_ailment: Store<MoveAilment>,
    move_battle_style: Store<MoveBattleStyle>,
    move_category: Store<ModelName>,
    move_damage_class: Store<MoveDamageClass>,
    move_learn_method: Store<MoveLearnMethod>,
    move_target: Store<MoveTarget>,
    nature: Store<Nature>,
    pal_park_area: Store<PalParkArea>,
    pokeathlon_stat: Store<PokeathlonStat>,
    pokedex: Store<Pokedex>,
    pokemon: Store<Pokemon>,
    pokemon_color: Store<PokemonColor>,
    pokemon_form: Store<PokemonForm>,
    pokemon_habitat: Store<PokemonHabitat>,
    pokemon_shape: Store<PokemonShape>,
    pokemon_species: Store<PokemonSpecies>,
    region: Store<Region>,
    stat: Store<Stat>,
    super_contest_effect: Store<SuperContestEffect>,
    r#type: Store<Type>,
    version: Store<Version>,
    version_group: Store<VersionGroup>,
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}

impl Client {
    pub fn new() -> Client {
        Self::new_with_url("https://pokeapi.co/").unwrap()
    }

    pub fn new_with_url(url: &str) -> Result<Client, url::ParseError> {
        let url = Url::parse(url)?;
        let url = url.join("api/v2/")?;
        Ok(Client {
            client: reqwest::Client::new(),
            url,
            load_chunk_size: 16,
            ability: Store::new(),
            berry: Store::new(),
            berry_firmness: Store::new(),
            berry_flavor: Store::new(),
            characteristic: Store::new(),
            contest_effect: Store::new(),
            contest_type: Store::new(),
            egg_group: Store::new(),
            encounter_condition: Store::new(),
            encounter_condition_value: Store::new(),
            encounter_method: Store::new(),
            evolution_chain: Store::new(),
            evolution_trigger: Store::new(),
            gender: Store::new(),
            generation: Store::new(),
            growth_rate: Store::new(),
            item: Store::new(),
            item_attribute: Store::new(),
            item_category: Store::new(),
            item_fling_effect: Store::new(),
            item_pocket: Store::new(),
            language: Store::new(),
            location: Store::new(),
            location_area: Store::new(),
            machine: Store::new(),
            r#move: Store::new(),
            move_ailment: Store::new(),
            move_battle_style: Store::new(),
            move_category: Store::new(),
            move_damage_class: Store::new(),
            move_learn_method: Store::new(),
            move_target: Store::new(),
            nature: Store::new(),
            pal_park_area: Store::new(),
            pokeathlon_stat: Store::new(),
            pokedex: Store::new(),
            pokemon: Store::new(),
            pokemon_color: Store::new(),
            pokemon_form: Store::new(),
            pokemon_habitat: Store::new(),
            pokemon_shape: Store::new(),
            pokemon_species: Store::new(),
            region: Store::new(),
            stat: Store::new(),
            super_contest_effect: Store::new(),
            r#type: Store::new(),
            version: Store::new(),
            version_group: Store::new(),
        })
    }

    async fn get_list_from_container<T>(
        &self,
        container: &Store<T>,
        name: &str,
    ) -> Result<Arc<ItemList<T>>, Error> {
        {
            let read = container.value.read().await;
            if let Some(v) = &read.value {
                return Ok(v.clone());
            }
        }
        {
            let mut write = container.value.write().await;
            let list = self.load_list::<T>(name).await?;
            write.value = Some(list.clone());
            Ok(list)
        }
    }

    async fn load_list<T>(&self, name: &str) -> Result<Arc<ItemList<T>>, Error> {
        let mut url = self.url.join(name)?;
        url.set_query(Some("offset=0&limit=20000"));
        let res = self.client.get(url).send().await?;
        let pokemon_list = res.json::<ItemListJson>().await?;
        let entry = Arc::new(ItemList::from(pokemon_list));
        Ok(entry)
    }
}

impl Client {
    pub async fn ability(&self) -> Result<Arc<ItemList<Ability>>, Error> {
        self.get_list_from_container(&self.ability, "ability").await
    }

    pub async fn berry(&self) -> Result<Arc<ItemList<Berry>>, Error> {
        self.get_list_from_container(&self.berry, "berry").await
    }

    pub async fn berry_firmness(&self) -> Result<Arc<ItemList<BerryFirmness>>, Error> {
        self.get_list_from_container(&self.berry_firmness, "berry-firmness")
            .await
    }

    pub async fn berry_flavor(&self) -> Result<Arc<ItemList<BerryFlavor>>, Error> {
        self.get_list_from_container(&self.berry_flavor, "berry-flavor")
            .await
    }

    pub async fn characteristic(&self) -> Result<Arc<ItemList<Characteristic>>, Error> {
        self.get_list_from_container(&self.characteristic, "characteristic")
            .await
    }

    pub async fn contest_effect(&self) -> Result<Arc<ItemList<ContestEffect>>, Error> {
        self.get_list_from_container(&self.contest_effect, "contest-effect")
            .await
    }

    pub async fn contest_type(&self) -> Result<Arc<ItemList<ContestType>>, Error> {
        self.get_list_from_container(&self.contest_type, "contest-type")
            .await
    }

    pub async fn egg_group(&self) -> Result<Arc<ItemList<EggGroup>>, Error> {
        self.get_list_from_container(&self.egg_group, "egg-group")
            .await
    }

    pub async fn encounter_condition(&self) -> Result<Arc<ItemList<EncounterCondition>>, Error> {
        self.get_list_from_container(&self.encounter_condition, "encounter-condition")
            .await
    }

    pub async fn encounter_condition_value(
        &self,
    ) -> Result<Arc<ItemList<EncounterConditionValue>>, Error> {
        self.get_list_from_container(&self.encounter_condition_value, "encounter-condition-value")
            .await
    }

    pub async fn encounter_method(&self) -> Result<Arc<ItemList<EncounterMethod>>, Error> {
        self.get_list_from_container(&self.encounter_method, "encounter-method")
            .await
    }

    pub async fn evolution_chain(&self) -> Result<Arc<ItemList<EvolutionChain>>, Error> {
        self.get_list_from_container(&self.evolution_chain, "evolution-chain")
            .await
    }

    pub async fn evolution_trigger(&self) -> Result<Arc<ItemList<EvolutionTrigger>>, Error> {
        self.get_list_from_container(&self.evolution_trigger, "evolution-trigger")
            .await
    }

    pub async fn gender(&self) -> Result<Arc<ItemList<Gender>>, Error> {
        self.get_list_from_container(&self.gender, "gender").await
    }

    pub async fn generation(&self) -> Result<Arc<ItemList<Generation>>, Error> {
        self.get_list_from_container(&self.generation, "generation")
            .await
    }

    pub async fn growth_rate(&self) -> Result<Arc<ItemList<GrowthRate>>, Error> {
        self.get_list_from_container(&self.growth_rate, "growth-rate")
            .await
    }

    pub async fn item(&self) -> Result<Arc<ItemList<Item>>, Error> {
        self.get_list_from_container(&self.item, "item").await
    }

    pub async fn item_attribute(&self) -> Result<Arc<ItemList<ItemAttribute>>, Error> {
        self.get_list_from_container(&self.item_attribute, "item-attribute")
            .await
    }

    pub async fn item_category(&self) -> Result<Arc<ItemList<ItemCategory>>, Error> {
        self.get_list_from_container(&self.item_category, "item-category")
            .await
    }

    pub async fn item_fling_effect(&self) -> Result<Arc<ItemList<ItemFlingEffect>>, Error> {
        self.get_list_from_container(&self.item_fling_effect, "item-fling-effect")
            .await
    }

    pub async fn item_pocket(&self) -> Result<Arc<ItemList<ItemPocket>>, Error> {
        self.get_list_from_container(&self.item_pocket, "item-pocket")
            .await
    }

    pub async fn language(&self) -> Result<Arc<ItemList<Language>>, Error> {
        self.get_list_from_container(&self.language, "language")
            .await
    }

    pub async fn location(&self) -> Result<Arc<ItemList<Location>>, Error> {
        self.get_list_from_container(&self.location, "location")
            .await
    }

    pub async fn location_area(&self) -> Result<Arc<ItemList<LocationArea>>, Error> {
        self.get_list_from_container(&self.location_area, "location-area")
            .await
    }

    pub async fn machine(&self) -> Result<Arc<ItemList<Machine>>, Error> {
        self.get_list_from_container(&self.machine, "machine").await
    }

    pub async fn r#move(&self) -> Result<Arc<ItemList<Move>>, Error> {
        self.get_list_from_container(&self.r#move, "move").await
    }

    pub async fn move_ailment(&self) -> Result<Arc<ItemList<MoveAilment>>, Error> {
        self.get_list_from_container(&self.move_ailment, "move-ailment")
            .await
    }

    pub async fn move_battle_style(&self) -> Result<Arc<ItemList<MoveBattleStyle>>, Error> {
        self.get_list_from_container(&self.move_battle_style, "move-battle-style")
            .await
    }

    pub async fn move_category(&self) -> Result<Arc<ItemList<ModelName>>, Error> {
        self.get_list_from_container(&self.move_category, "move-category")
            .await
    }

    pub async fn move_damage_class(&self) -> Result<Arc<ItemList<MoveDamageClass>>, Error> {
        self.get_list_from_container(&self.move_damage_class, "move-damage-class")
            .await
    }

    pub async fn move_learn_method(&self) -> Result<Arc<ItemList<MoveLearnMethod>>, Error> {
        self.get_list_from_container(&self.move_learn_method, "move-learn-method")
            .await
    }

    pub async fn move_target(&self) -> Result<Arc<ItemList<MoveTarget>>, Error> {
        self.get_list_from_container(&self.move_target, "move-target")
            .await
    }

    pub async fn nature(&self) -> Result<Arc<ItemList<Nature>>, Error> {
        self.get_list_from_container(&self.nature, "nature").await
    }

    pub async fn pal_park_area(&self) -> Result<Arc<ItemList<PalParkArea>>, Error> {
        self.get_list_from_container(&self.pal_park_area, "pal-park-area")
            .await
    }

    pub async fn pokeathlon_stat(&self) -> Result<Arc<ItemList<PokeathlonStat>>, Error> {
        self.get_list_from_container(&self.pokeathlon_stat, "pokeathlon-stat")
            .await
    }

    pub async fn pokedex(&self) -> Result<Arc<ItemList<Pokedex>>, Error> {
        self.get_list_from_container(&self.pokedex, "pokedex").await
    }

    pub async fn pokemon(&self) -> Result<Arc<ItemList<Pokemon>>, Error> {
        self.get_list_from_container(&self.pokemon, "pokemon").await
    }

    pub async fn pokemon_color(&self) -> Result<Arc<ItemList<PokemonColor>>, Error> {
        self.get_list_from_container(&self.pokemon_color, "pokemon-color")
            .await
    }

    pub async fn pokemon_form(&self) -> Result<Arc<ItemList<PokemonForm>>, Error> {
        self.get_list_from_container(&self.pokemon_form, "pokemon-form")
            .await
    }

    pub async fn pokemon_habitat(&self) -> Result<Arc<ItemList<PokemonHabitat>>, Error> {
        self.get_list_from_container(&self.pokemon_habitat, "pokemon-habitat")
            .await
    }

    pub async fn pokemon_shape(&self) -> Result<Arc<ItemList<PokemonShape>>, Error> {
        self.get_list_from_container(&self.pokemon_shape, "pokemon-shape")
            .await
    }

    pub async fn pokemon_species(&self) -> Result<Arc<ItemList<PokemonSpecies>>, Error> {
        self.get_list_from_container(&self.pokemon_species, "pokemon-species")
            .await
    }

    pub async fn region(&self) -> Result<Arc<ItemList<Region>>, Error> {
        self.get_list_from_container(&self.region, "region").await
    }

    pub async fn stat(&self) -> Result<Arc<ItemList<Stat>>, Error> {
        self.get_list_from_container(&self.stat, "stat").await
    }

    pub async fn super_contest_effect(&self) -> Result<Arc<ItemList<SuperContestEffect>>, Error> {
        self.get_list_from_container(&self.super_contest_effect, "super-contest-effect")
            .await
    }

    pub async fn r#type(&self) -> Result<Arc<ItemList<Type>>, Error> {
        self.get_list_from_container(&self.r#type, "type").await
    }

    pub async fn version(&self) -> Result<Arc<ItemList<Version>>, Error> {
        self.get_list_from_container(&self.version, "version").await
    }

    pub async fn version_group(&self) -> Result<Arc<ItemList<VersionGroup>>, Error> {
        self.get_list_from_container(&self.version_group, "version-group")
            .await
    }
}
