use crate::berries::berries::Berry;
use crate::berries::berry_firmnesses::BerryFirmness;
use crate::berries::berry_flavors::BerryFlavor;
use crate::contests::contest_effects::ContestEffect;
use crate::contests::contest_types::ContestType;
use crate::contests::super_contest_effects::SuperContestEffect;
use crate::encounters::encounter_condition_values::EncounterConditionValue;
use crate::encounters::encounter_conditions::EncounterCondition;
use crate::encounters::encounter_methods::EncounterMethod;
use crate::error::Error;
use crate::evolution::evolution_chains::EvolutionChain;
use crate::evolution::evolution_triggers::EvolutionTrigger;
use crate::games::generations::Generation;
use crate::games::pokedexes::Pokedex;
use crate::games::version::Version;
use crate::games::version_groups::VersionGroup;
use crate::items::item::Item;
use crate::items::item_attributes::ItemAttribute;
use crate::items::item_categories::ItemCategory;
use crate::items::item_fling_effects::ItemFlingEffect;
use crate::items::item_pockets::ItemPocket;
use crate::locations::location_areas::LocationArea;
use crate::locations::locations::Location;
use crate::locations::pal_park_areas::PalParkArea;
use crate::locations::regions::Region;
use crate::machines::machines::Machine;
use crate::moves::move_ailments::MoveAilment;
use crate::moves::move_battle_styles::MoveBattleStyle;
use crate::moves::move_categories::ModelName;
use crate::moves::move_damage_classes::MoveDamageClass;
use crate::moves::move_learn_methods::MoveLearnMethod;
use crate::moves::move_targets::MoveTarget;
use crate::moves::moves::Move;
use crate::pokemon::abilities::Ability;
use crate::pokemon::characteristics::Characteristic;
use crate::pokemon::egg_groups::EggGroup;
use crate::pokemon::genders::Gender;
use crate::pokemon::growth_rates::GrowthRate;
use crate::pokemon::natures::Nature;
use crate::pokemon::pokeathlon_stats::PokeathlonStat;
use crate::pokemon::pokemon::Pokemon;
use crate::pokemon::pokemon_colors::PokemonColor;
use crate::pokemon::pokemon_forms::PokemonForm;
use crate::pokemon::pokemon_habitats::PokemonHabitat;
use crate::pokemon::pokemon_shapes::PokemonShape;
use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::pokemon::stats::Stat;
use crate::pokemon::types::Type;
use crate::utility::languages::Language;
use crate::Client;
use serde::Deserialize;
use std::sync::Arc;
use std::{fmt::Display, marker::PhantomData};

#[derive(Deserialize, Debug)]
#[serde(transparent)]
pub struct Id<T> {
    value: i32,
    #[serde(skip)]
    data_type: PhantomData<T>,
}

impl<T> Id<T> {
    pub fn get(&self) -> i32 {
        self.value
    }

    pub fn new(value: i32) -> Self {
        Self {
            value,
            data_type: Default::default(),
        }
    }
}

impl<T> Clone for Id<T> {
    fn clone(&self) -> Self {
        Self {
            value: self.value,
            data_type: self.data_type,
        }
    }
}

impl<T> Copy for Id<T> {}

impl<T> Display for Id<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl Id<Ability> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Ability>, Error> {
        client.ability().await?.id(self.get(), client).await
    }
}

impl Id<Berry> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Berry>, Error> {
        client.berry().await?.id(self.get(), client).await
    }
}

impl Id<BerryFirmness> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<BerryFirmness>, Error> {
        client.berry_firmness().await?.id(self.get(), client).await
    }
}

impl Id<BerryFlavor> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<BerryFlavor>, Error> {
        client.berry_flavor().await?.id(self.get(), client).await
    }
}

impl Id<Characteristic> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Characteristic>, Error> {
        client.characteristic().await?.id(self.get(), client).await
    }
}

impl Id<ContestEffect> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<ContestEffect>, Error> {
        client.contest_effect().await?.id(self.get(), client).await
    }
}

impl Id<ContestType> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<ContestType>, Error> {
        client.contest_type().await?.id(self.get(), client).await
    }
}

impl Id<EggGroup> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<EggGroup>, Error> {
        client.egg_group().await?.id(self.get(), client).await
    }
}

impl Id<EncounterCondition> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<EncounterCondition>, Error> {
        client
            .encounter_condition()
            .await?
            .id(self.get(), client)
            .await
    }
}

impl Id<EncounterConditionValue> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<EncounterConditionValue>, Error> {
        client
            .encounter_condition_value()
            .await?
            .id(self.get(), client)
            .await
    }
}

impl Id<EncounterMethod> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<EncounterMethod>, Error> {
        client
            .encounter_method()
            .await?
            .id(self.get(), client)
            .await
    }
}

impl Id<EvolutionChain> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<EvolutionChain>, Error> {
        client.evolution_chain().await?.id(self.get(), client).await
    }
}

impl Id<EvolutionTrigger> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<EvolutionTrigger>, Error> {
        client
            .evolution_trigger()
            .await?
            .id(self.get(), client)
            .await
    }
}

impl Id<Gender> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Gender>, Error> {
        client.gender().await?.id(self.get(), client).await
    }
}

impl Id<Generation> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Generation>, Error> {
        client.generation().await?.id(self.get(), client).await
    }
}

impl Id<GrowthRate> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<GrowthRate>, Error> {
        client.growth_rate().await?.id(self.get(), client).await
    }
}

impl Id<Item> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Item>, Error> {
        client.item().await?.id(self.get(), client).await
    }
}

impl Id<ItemAttribute> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<ItemAttribute>, Error> {
        client.item_attribute().await?.id(self.get(), client).await
    }
}

impl Id<ItemCategory> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<ItemCategory>, Error> {
        client.item_category().await?.id(self.get(), client).await
    }
}

impl Id<ItemFlingEffect> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<ItemFlingEffect>, Error> {
        client
            .item_fling_effect()
            .await?
            .id(self.get(), client)
            .await
    }
}

impl Id<ItemPocket> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<ItemPocket>, Error> {
        client.item_pocket().await?.id(self.get(), client).await
    }
}

impl Id<Language> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Language>, Error> {
        client.language().await?.id(self.get(), client).await
    }
}

impl Id<Location> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Location>, Error> {
        client.location().await?.id(self.get(), client).await
    }
}

impl Id<LocationArea> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<LocationArea>, Error> {
        client.location_area().await?.id(self.get(), client).await
    }
}

impl Id<Machine> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Machine>, Error> {
        client.machine().await?.id(self.get(), client).await
    }
}

impl Id<Move> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Move>, Error> {
        client.r#move().await?.id(self.get(), client).await
    }
}

impl Id<MoveAilment> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<MoveAilment>, Error> {
        client.move_ailment().await?.id(self.get(), client).await
    }
}

impl Id<MoveBattleStyle> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<MoveBattleStyle>, Error> {
        client
            .move_battle_style()
            .await?
            .id(self.get(), client)
            .await
    }
}

impl Id<ModelName> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<ModelName>, Error> {
        client.move_category().await?.id(self.get(), client).await
    }
}

impl Id<MoveDamageClass> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<MoveDamageClass>, Error> {
        client
            .move_damage_class()
            .await?
            .id(self.get(), client)
            .await
    }
}

impl Id<MoveLearnMethod> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<MoveLearnMethod>, Error> {
        client
            .move_learn_method()
            .await?
            .id(self.get(), client)
            .await
    }
}

impl Id<MoveTarget> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<MoveTarget>, Error> {
        client.move_target().await?.id(self.get(), client).await
    }
}

impl Id<Nature> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Nature>, Error> {
        client.nature().await?.id(self.get(), client).await
    }
}

impl Id<PalParkArea> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<PalParkArea>, Error> {
        client.pal_park_area().await?.id(self.get(), client).await
    }
}

impl Id<PokeathlonStat> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<PokeathlonStat>, Error> {
        client.pokeathlon_stat().await?.id(self.get(), client).await
    }
}

impl Id<Pokedex> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Pokedex>, Error> {
        client.pokedex().await?.id(self.get(), client).await
    }
}

impl Id<Pokemon> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Pokemon>, Error> {
        client.pokemon().await?.id(self.get(), client).await
    }
}

impl Id<PokemonColor> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<PokemonColor>, Error> {
        client.pokemon_color().await?.id(self.get(), client).await
    }
}

impl Id<PokemonForm> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<PokemonForm>, Error> {
        client.pokemon_form().await?.id(self.get(), client).await
    }
}

impl Id<PokemonHabitat> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<PokemonHabitat>, Error> {
        client.pokemon_habitat().await?.id(self.get(), client).await
    }
}

impl Id<PokemonShape> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<PokemonShape>, Error> {
        client.pokemon_shape().await?.id(self.get(), client).await
    }
}

impl Id<PokemonSpecies> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<PokemonSpecies>, Error> {
        client.pokemon_species().await?.id(self.get(), client).await
    }
}

impl Id<Region> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Region>, Error> {
        client.region().await?.id(self.get(), client).await
    }
}

impl Id<Stat> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Stat>, Error> {
        client.stat().await?.id(self.get(), client).await
    }
}

impl Id<SuperContestEffect> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<SuperContestEffect>, Error> {
        client
            .super_contest_effect()
            .await?
            .id(self.get(), client)
            .await
    }
}

impl Id<Type> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Type>, Error> {
        client.r#type().await?.id(self.get(), client).await
    }
}

impl Id<Version> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<Version>, Error> {
        client.version().await?.id(self.get(), client).await
    }
}

impl Id<VersionGroup> {
    pub async fn load_data(&self, client: &Client) -> Result<Arc<VersionGroup>, Error> {
        client.version_group().await?.id(self.get(), client).await
    }
}
