use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#pokemon-colors)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonColor {
    pub id: Id<PokemonColor>,
    pub name: String,
    pub names: Vec<Name>,
    pub pokemon_species: Vec<NamedAPIResource<PokemonSpecies>>,
}

#[async_trait]
impl LoadUrl for PokemonColor {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.pokemon_color().await?.url(url, client).await
    }
}
