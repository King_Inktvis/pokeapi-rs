use crate::berries::berry_flavors::BerryFlavor;
use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::moves::move_battle_styles::MoveBattleStyle;
use crate::pokemon::pokeathlon_stats::PokeathlonStat;
use crate::pokemon::stats::Stat;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#natures)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Nature {
    pub id: Id<Nature>,
    pub name: String,
    pub decreased_stat: Option<NamedAPIResource<Stat>>,
    pub increased_stat: Option<NamedAPIResource<Stat>>,
    pub hates_flavor: Option<NamedAPIResource<BerryFlavor>>,
    pub likes_flavor: Option<NamedAPIResource<BerryFlavor>>,
    pub pokeathlon_stat_changes: Vec<NatureStatChange>,
    pub move_battle_style_preferences: Vec<MoveBattleStylePreference>,
    pub names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for Nature {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.nature().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct NatureStatChange {
    pub max_change: i8,
    pub pokeathlon_stat: NamedAPIResource<PokeathlonStat>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MoveBattleStylePreference {
    pub low_hp_preference: u8,
    pub high_hp_preference: u8,
    pub move_battle_style: NamedAPIResource<MoveBattleStyle>,
}
