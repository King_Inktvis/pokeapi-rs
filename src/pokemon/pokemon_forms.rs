use crate::error::Error;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::LoadUrl;
use crate::pokemon::pokemon::Pokemon;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#pokemon-forms)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonForm {
    pub id: Id<PokemonForm>,
    pub name: String,
    pub order: u16,
    pub form_order: u16,
    pub is_default: bool,
    pub is_battle_only: bool,
    pub is_mega: bool,
    pub form_name: String,
    pub pokemon: NamedAPIResource<Pokemon>,
    pub sprites: PokemonFormSprites,
    pub version_group: NamedAPIResource<VersionGroup>,
    pub names: Vec<Name>,
    pub form_names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for PokemonForm {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.pokemon_form().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonFormSprites {
    pub front_default: Option<String>,
    pub front_shiny: Option<String>,
    pub back_default: Option<String>,
    pub back_shiny: Option<String>,
}
