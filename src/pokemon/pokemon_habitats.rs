use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#pokemon-habitats)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonHabitat {
    pub id: Id<PokemonHabitat>,
    pub name: String,
    pub names: Vec<Name>,
    pub pokemon_species: Vec<NamedAPIResource<PokemonSpecies>>,
}

#[async_trait]
impl LoadUrl for PokemonHabitat {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.pokemon_habitat().await?.url(url, client).await
    }
}
