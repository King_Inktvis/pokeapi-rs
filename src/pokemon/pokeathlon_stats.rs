use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::pokemon::natures::Nature;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#pokeathlon-stats)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokeathlonStat {
    pub id: Id<PokeathlonStat>,
    pub name: String,
    pub names: Vec<Name>,
    pub affecting_natures: NaturePokeathlonStatAffectSets,
}

#[async_trait]
impl LoadUrl for PokeathlonStat {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.pokeathlon_stat().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct NaturePokeathlonStatAffectSets {
    pub increase: Vec<NaturePokeathlonStatAffect>,
    pub decrease: Vec<NaturePokeathlonStatAffect>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct NaturePokeathlonStatAffect {
    pub max_change: i8,
    pub nature: NamedAPIResource<Nature>,
}
