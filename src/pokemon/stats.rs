use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::moves::move_damage_classes::MoveDamageClass;
use crate::moves::moves::Move;
use crate::pokemon::characteristics::Characteristic;
use crate::pokemon::natures::Nature;
use crate::types::Id;
use crate::utility::common_models::{APIResource, Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#stats)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Stat {
    pub id: Id<Stat>,
    pub name: String,
    pub game_index: u8,
    pub is_battle_only: bool,
    pub affecting_moves: MoveStatAffectSets,
    pub affecting_natures: NatureStatAffectSets,
    pub characteristics: Vec<APIResource<Characteristic>>,
    pub move_damage_class: Option<NamedAPIResource<MoveDamageClass>>,
    pub names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for Stat {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.stat().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MoveStatAffectSets {
    pub increase: Vec<MoveStatAffect>,
    pub decrease: Vec<MoveStatAffect>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MoveStatAffect {
    pub change: i8,
    pub r#move: NamedAPIResource<Move>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct NatureStatAffectSets {
    pub increase: Vec<NamedAPIResource<Nature>>,
    pub decrease: Vec<NamedAPIResource<Nature>>,
}
