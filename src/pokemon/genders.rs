use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::types::Id;
use crate::utility::common_models::NamedAPIResource;
use macros::Getters;
use serde::Deserialize;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#genders)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Gender {
    pub id: Id<Gender>,
    pub name: String,
    pub pokemon_species_details: Vec<PokemonSpeciesGender>,
    pub required_for_evolution: Vec<NamedAPIResource<PokemonSpecies>>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonSpeciesGender {
    rate: i8,
    pokemon_species: NamedAPIResource<PokemonSpecies>,
}
