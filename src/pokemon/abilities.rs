use crate::error::Error;
use crate::games::generations::Generation;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::LoadUrl;
use crate::pokemon::pokemon::Pokemon;
use crate::types::Id;
use crate::utility::common_models::{Effect, Name, NamedAPIResource, VerboseEffect};
use crate::utility::languages::Language;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#abilities)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Ability {
    pub id: Id<Ability>,
    pub name: String,
    pub is_main_series: bool,
    pub generation: NamedAPIResource<Generation>,
    pub names: Vec<Name>,
    pub effect_entries: Vec<VerboseEffect>,
    pub effect_changes: Vec<AbilityEffectChange>,
    pub flavor_text_entries: Vec<AbilityFlavorText>,
    pub pokemon: Vec<AbilityPokemon>,
}

#[async_trait]
impl LoadUrl for Ability {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.ability().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct AbilityEffectChange {
    pub effect_entries: Vec<Effect>,
    pub version_group: NamedAPIResource<VersionGroup>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct AbilityFlavorText {
    pub flavor_text: String,
    pub language: NamedAPIResource<Language>,
    pub version_group: NamedAPIResource<VersionGroup>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct AbilityPokemon {
    pub is_hidden: bool,
    pub slot: u8,
    pub pokemon: NamedAPIResource<Pokemon>,
}
