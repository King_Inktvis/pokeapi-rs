use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::types::Id;
use crate::utility::common_models::{Description, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#growth-rates)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct GrowthRate {
    pub id: Id<GrowthRate>,
    pub name: String,
    pub formula: String,
    pub descriptions: Vec<Description>,
    pub levels: Vec<GrowthRateExperience>,
    pub pokemon_species: Vec<NamedAPIResource<PokemonSpecies>>,
}

#[async_trait]
impl LoadUrl for GrowthRate {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.growth_rate().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct GrowthRateExperience {
    pub level: u8,
    pub experience: i32,
}
