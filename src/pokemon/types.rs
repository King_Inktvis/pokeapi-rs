use crate::error::Error;
use crate::games::generations::Generation;
use crate::group_containers::LoadUrl;
use crate::moves::move_damage_classes::MoveDamageClass;
use crate::moves::moves::Move;
use crate::pokemon::pokemon::Pokemon;
use crate::types::Id;
use crate::utility::common_models::{GenerationGameIndex, Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#types)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Type {
    pub id: Id<Type>,
    pub name: String,
    pub damage_relations: TypeRelations,
    pub game_indices: Vec<GenerationGameIndex>,
    pub generation: NamedAPIResource<Generation>,
    pub move_damage_class: Option<NamedAPIResource<MoveDamageClass>>,
    pub names: Vec<Name>,
    pub pokemon: Vec<TypePokemon>,
    pub moves: Vec<NamedAPIResource<Move>>,
}

#[async_trait]
impl LoadUrl for Type {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.r#type().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct TypeRelations {
    pub no_damage_to: Vec<NamedAPIResource<Type>>,
    pub half_damage_to: Vec<NamedAPIResource<Type>>,
    pub double_damage_to: Vec<NamedAPIResource<Type>>,
    pub no_damage_from: Vec<NamedAPIResource<Type>>,
    pub half_damage_from: Vec<NamedAPIResource<Type>>,
    pub double_damage_from: Vec<NamedAPIResource<Type>>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct TypePokemon {
    pub slot: u8,
    pub pokemon: NamedAPIResource<Pokemon>,
}
