use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::types::Id;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#characteristics)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Characteristic {
    pub id: Id<Characteristic>,
    pub gene_modulo: u8,
    pub possible_values: Vec<u8>,
}

#[async_trait]
impl LoadUrl for Characteristic {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.characteristic().await?.url(url, client).await
    }
}
