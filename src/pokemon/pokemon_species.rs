use crate::error::Error;
use crate::evolution::evolution_chains::EvolutionChain;
use crate::games::generations::Generation;
use crate::games::pokedexes::Pokedex;
use crate::group_containers::LoadUrl;
use crate::locations::location_areas::LocationArea;
use crate::pokemon::egg_groups::EggGroup;
use crate::pokemon::growth_rates::GrowthRate;
use crate::pokemon::pokemon::Pokemon;
use crate::pokemon::pokemon_colors::PokemonColor;
use crate::pokemon::pokemon_habitats::PokemonHabitat;
use crate::pokemon::pokemon_shapes::PokemonShape;
use crate::types::Id;
use crate::utility::common_models::{APIResource, Description, FlavorText, Name, NamedAPIResource};
use crate::utility::languages::Language;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#pokemon-species)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonSpecies {
    pub id: Id<PokemonSpecies>,
    pub name: String,
    pub order: u16,
    pub gender_rate: i8,
    pub capture_rate: u8,
    pub base_happiness: Option<u8>,
    pub is_baby: bool,
    pub hatch_counter: Option<u8>,
    pub has_gender_differences: bool,
    pub forms_switchable: bool,
    pub growth_rate: NamedAPIResource<GrowthRate>,
    pub pokedex_numbers: Vec<PokemonSpeciesDexEntry>,
    pub egg_groups: Vec<NamedAPIResource<EggGroup>>,
    pub color: NamedAPIResource<PokemonColor>,
    pub shape: Option<NamedAPIResource<PokemonShape>>,
    pub evolves_from_species: Option<NamedAPIResource<PokemonSpecies>>,
    pub evolution_chain: Option<APIResource<EvolutionChain>>,
    pub habitat: Option<NamedAPIResource<PokemonHabitat>>,
    pub generation: NamedAPIResource<Generation>,
    pub names: Vec<Name>,
    pub pal_park_encounters: Vec<PalParkEncounterArea>,
    pub flavor_text_entries: Vec<FlavorText>,
    pub form_descriptions: Vec<Description>,
    pub genera: Vec<Genus>,
    pub varieties: Vec<PokemonSpeciesVariety>,
}

#[async_trait]
impl LoadUrl for PokemonSpecies {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.pokemon_species().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonSpeciesDexEntry {
    pub entry_number: u16,
    pub pokedex: NamedAPIResource<Pokedex>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PalParkEncounterArea {
    pub base_score: u8,
    pub rate: u8,
    pub area: NamedAPIResource<LocationArea>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Genus {
    pub genus: String,
    pub language: NamedAPIResource<Language>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonSpeciesVariety {
    pub is_default: bool,
    pub pokemon: NamedAPIResource<Pokemon>,
}
