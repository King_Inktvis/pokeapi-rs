use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::utility::languages::Language;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#pokemon-shapes)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonShape {
    pub id: Id<PokemonShape>,
    pub name: String,
    pub awesome_names: Vec<AwesomeName>,
    pub names: Vec<Name>,
    pub pokemon_species: Vec<NamedAPIResource<PokemonSpecies>>,
}

#[async_trait]
impl LoadUrl for PokemonShape {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.pokemon_shape().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct AwesomeName {
    pub awesome_name: String,
    pub language: NamedAPIResource<Language>,
}
