use crate::error::Error;
use crate::games::version::Version;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::LoadUrl;
use crate::items::item::Item;
use crate::moves::move_learn_methods::MoveLearnMethod;
use crate::moves::moves::Move;
use crate::pokemon::abilities::Ability;
use crate::pokemon::pokemon_forms::PokemonForm;
use crate::pokemon::pokemon_species::PokemonSpecies;
use crate::pokemon::stats::Stat;
use crate::pokemon::types::Type;
use crate::types::Id;
use crate::utility::common_models::{NamedAPIResource, VersionGameIndex};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#pokemon)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Pokemon {
    pub id: Id<Pokemon>,
    pub name: String,
    pub base_experience: Option<u16>,
    pub height: u16,
    pub is_default: bool,
    pub order: i16,
    pub weight: u16,
    pub abilities: Vec<PokemonAbility>,
    pub forms: Vec<NamedAPIResource<PokemonForm>>,
    pub game_indices: Vec<VersionGameIndex>,
    pub held_items: Vec<PokemonHeldItem>,
    pub location_area_encounters: String,
    pub moves: Vec<PokemonMove>,
    pub sprites: PokemonSprites,
    pub species: NamedAPIResource<PokemonSpecies>,
    pub stats: Vec<PokemonStat>,
    pub types: Vec<PokemonType>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonAbility {
    pub is_hidden: bool,
    pub slot: i32,
    pub ability: NamedAPIResource<Ability>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonType {
    pub slot: u8,
    pub r#type: NamedAPIResource<Type>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonHeldItem {
    pub item: NamedAPIResource<Item>,
    pub version_details: Vec<PokemonHeldItemVersion>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonHeldItemVersion {
    pub version: NamedAPIResource<Version>,
    pub rarity: u8,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonMove {
    pub r#move: NamedAPIResource<Move>,
    pub version_group_details: Vec<PokemonMoveVersion>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonMoveVersion {
    pub move_learn_method: NamedAPIResource<MoveLearnMethod>,
    pub version_group: NamedAPIResource<VersionGroup>,
    pub level_learned_at: u8,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonStat {
    pub stat: NamedAPIResource<Stat>,
    pub effort: u8,
    pub base_stat: u8,
}

#[async_trait]
impl LoadUrl for Pokemon {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Pokemon>, Error> {
        client.pokemon().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PokemonSprites {
    pub front_default: Option<String>,
    pub front_shiny: Option<String>,
    pub front_female: Option<String>,
    pub front_shiny_female: Option<String>,
    pub back_default: Option<String>,
    pub back_shiny: Option<String>,
    pub back_female: Option<String>,
    pub back_shiny_female: Option<String>,
}
