use crate::contests::contest_effects::ContestEffect;
use crate::contests::contest_types::ContestType;
use crate::contests::super_contest_effects::SuperContestEffect;
use crate::error::Error;
use crate::games::generations::Generation;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::LoadUrl;
use crate::moves::move_ailments::MoveAilment;
use crate::moves::move_categories::ModelName;
use crate::moves::move_damage_classes::MoveDamageClass;
use crate::moves::move_targets::MoveTarget;
use crate::pokemon::abilities::AbilityEffectChange;
use crate::pokemon::stats::Stat;
use crate::pokemon::types::Type;
use crate::types::Id;
use crate::utility::common_models::{
    APIResource, MachineVersionDetail, Name, NamedAPIResource, VerboseEffect,
};
use crate::utility::languages::Language;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#moves)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct Move {
    pub id: Id<Move>,
    pub name: String,
    pub accuracy: Option<u8>,
    pub effect_chance: Option<u8>,
    pub pp: Option<u8>,
    pub priority: i8,
    pub power: Option<u8>,
    pub contest_combos: Option<ContestComboSets>,
    pub contest_type: Option<NamedAPIResource<ContestType>>,
    pub contest_effect: Option<APIResource<ContestEffect>>,
    pub damage_class: Option<NamedAPIResource<MoveDamageClass>>,
    pub effect_entries: Vec<VerboseEffect>,
    pub effect_changes: Vec<AbilityEffectChange>,
    pub flavor_text_entries: Vec<MoveFlavorText>,
    pub generation: NamedAPIResource<Generation>,
    pub machines: Vec<MachineVersionDetail>,
    pub meta: Option<MoveMetaData>,
    pub names: Vec<Name>,
    pub past_values: Vec<PastMoveStatValues>,
    pub stat_changes: Vec<MoveStatChange>,
    pub super_contest_effect: Option<APIResource<SuperContestEffect>>,
    pub target: NamedAPIResource<MoveTarget>,
    pub r#type: NamedAPIResource<Type>,
}

#[async_trait]
impl LoadUrl for Move {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.r#move().await?.url(url, client).await
    }
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ContestComboSets {
    pub normal: ContestComboDetail,
    #[serde(rename = "super")]
    pub _super: ContestComboDetail,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ContestComboDetail {
    pub use_before: Option<Vec<NamedAPIResource<Move>>>,
    pub use_after: Option<Vec<NamedAPIResource<Move>>>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MoveMetaData {
    pub ailment: NamedAPIResource<MoveAilment>,
    pub category: NamedAPIResource<ModelName>,
    pub min_hits: Option<u8>,
    pub max_hits: Option<u8>,
    pub min_turns: Option<u8>,
    pub max_turns: Option<u8>,
    pub drain: i8,
    pub healing: i8,
    pub crit_rate: u8,
    pub ailment_chance: u8,
    pub flinch_chance: u8,
    pub stat_chance: u8,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct PastMoveStatValues {
    pub accuracy: Option<u8>,
    pub effect_chance: Option<u8>,
    pub power: Option<u8>,
    pub pp: Option<u8>,
    pub effect_entries: Vec<VerboseEffect>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MoveStatChange {
    pub change: i8,
    pub stat: NamedAPIResource<Stat>,
}

#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MoveFlavorText {
    pub flavor_text: String,
    pub language: NamedAPIResource<Language>,
    pub version_group: NamedAPIResource<VersionGroup>,
}
