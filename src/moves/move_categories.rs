use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::moves::moves::Move;
use crate::types::Id;
use crate::utility::common_models::{Description, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#move-categories)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct ModelName {
    pub id: Id<ModelName>,
    pub name: String,
    pub moves: Vec<NamedAPIResource<Move>>,
    pub descriptions: Vec<Description>,
}

#[async_trait]
impl LoadUrl for ModelName {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.move_category().await?.url(url, client).await
    }
}
