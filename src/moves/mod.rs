//! [PokéAPI docs](https://pokeapi.co/docs/v2#moves-section)
pub mod move_ailments;
pub mod move_battle_styles;
pub mod move_categories;
pub mod move_damage_classes;
pub mod move_learn_methods;
pub mod move_targets;
pub mod moves;
