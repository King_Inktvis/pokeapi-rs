use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::moves::moves::Move;
use crate::types::Id;
use crate::utility::common_models::{Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#move-ailments)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MoveAilment {
    pub id: Id<MoveAilment>,
    pub name: String,
    pub moves: Vec<NamedAPIResource<Move>>,
    pub names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for MoveAilment {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.move_ailment().await?.url(url, client).await
    }
}
