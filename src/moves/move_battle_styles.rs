use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::types::Id;
use crate::utility::common_models::Name;
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#move-battle-styles)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MoveBattleStyle {
    pub id: Id<MoveBattleStyle>,
    pub name: String,
    pub names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for MoveBattleStyle {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.move_battle_style().await?.url(url, client).await
    }
}
