use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::moves::moves::Move;
use crate::types::Id;
use crate::utility::common_models::{Description, Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#move-damage-classes)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MoveDamageClass {
    pub id: Id<MoveDamageClass>,
    pub name: String,
    pub descriptions: Vec<Description>,
    pub moves: Vec<NamedAPIResource<Move>>,
    pub names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for MoveDamageClass {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.move_damage_class().await?.url(url, client).await
    }
}
