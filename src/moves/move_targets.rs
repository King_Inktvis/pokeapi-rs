use crate::error::Error;
use crate::group_containers::LoadUrl;
use crate::moves::moves::Move;
use crate::types::Id;
use crate::utility::common_models::{Description, Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#move-targets)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MoveTarget {
    pub id: Id<MoveTarget>,
    pub name: String,
    pub descriptions: Vec<Description>,
    pub moves: Vec<NamedAPIResource<Move>>,
    pub names: Vec<Name>,
}

#[async_trait]
impl LoadUrl for MoveTarget {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.move_target().await?.url(url, client).await
    }
}
