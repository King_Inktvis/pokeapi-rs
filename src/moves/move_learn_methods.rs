use crate::error::Error;
use crate::games::version_groups::VersionGroup;
use crate::group_containers::LoadUrl;
use crate::types::Id;
use crate::utility::common_models::{Description, Name, NamedAPIResource};
use crate::Client;
use async_trait::async_trait;
use macros::Getters;
use serde::Deserialize;
use std::sync::Arc;

/// [PokéAPI docs](https://pokeapi.co/docs/v2#move-learn-methods)
#[derive(Debug, Deserialize, Clone, Getters)]
pub struct MoveLearnMethod {
    pub id: Id<MoveLearnMethod>,
    pub name: String,
    pub descriptions: Vec<Description>,
    pub names: Vec<Name>,
    pub version_groups: Vec<NamedAPIResource<VersionGroup>>,
}

#[async_trait]
impl LoadUrl for MoveLearnMethod {
    async fn load_url(url: &str, client: &Client) -> Result<Arc<Self>, Error> {
        client.move_learn_method().await?.url(url, client).await
    }
}
