mod berries;
mod config;
mod contests;
mod encounters;
mod evolution;
mod games;
mod items;
mod locations;
mod machines;
mod moves;
mod pokemon;
mod utility;

use config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn load_pokemon_list() {
    let client = Client::new_with_url(&URL).unwrap();
    let res = client.pokemon().await;
    assert!(res.is_ok());
    let result = res.unwrap();
    assert_eq!(result.count as usize, result.results.len());
}

#[tokio::test]
async fn get_dragonite() {
    let client = Client::new_with_url(&URL).unwrap();
    let pokemon = client.pokemon().await;
    assert!(pokemon.is_ok());
    let res = pokemon.unwrap().name("dragonite", &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_dragonite_from_id() {
    let client = Client::new_with_url(&URL).unwrap();
    let pokemon = client.pokemon().await.unwrap();
    let res = pokemon.id(149, &client).await;
    assert!(res.is_ok());
    assert_eq!(res.unwrap().name(), "dragonite");
}

#[tokio::test]
async fn get_ability() {
    let client = Client::new_with_url(&URL).unwrap();
    let ability = client.ability().await.unwrap();
    let res = ability.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_berry() {
    let client = Client::new_with_url(&URL).unwrap();
    let berry = client.berry().await.unwrap();
    let res = berry.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_berry_firmness() {
    let client = Client::new_with_url(&URL).unwrap();
    let berry_firmness = client.berry_firmness().await.unwrap();
    let res = berry_firmness.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_characteristic() {
    let client = Client::new_with_url(&URL).unwrap();
    let characteristics = client.characteristic().await.unwrap();
    let res = characteristics.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_contest_effect() {
    let client = Client::new_with_url(&URL).unwrap();
    let contest_effect = client.contest_effect().await.unwrap();
    let res = contest_effect.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_contest_type() {
    let client = Client::new_with_url(&URL).unwrap();
    let contest_type = client.contest_type().await.unwrap();
    let res = contest_type.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_egg_group() {
    let client = Client::new_with_url(&URL).unwrap();
    let egg_group = client.egg_group().await.unwrap();
    let res = egg_group.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_encounter_condition() {
    let client = Client::new_with_url(&URL).unwrap();
    let encounter_condition = client.encounter_condition().await.unwrap();
    let res = encounter_condition.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_encounter_condition_value() {
    let client = Client::new_with_url(&URL).unwrap();
    let value = client.encounter_condition_value().await.unwrap();
    let res = value.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_encounter_method() {
    let client = Client::new_with_url(&URL).unwrap();
    let encounter_method = client.encounter_method().await.unwrap();
    let res = encounter_method.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_evolution_chain() {
    let client = Client::new_with_url(&URL).unwrap();
    let chain = client.evolution_chain().await.unwrap();
    let res = chain.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_evolution_trigger() {
    let client = Client::new_with_url(&URL).unwrap();
    let trigger = client.evolution_trigger().await.unwrap();
    let res = trigger.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_gender() {
    let client = Client::new_with_url(&URL).unwrap();
    let gender = client.gender().await.unwrap();
    let res = gender.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_genderation() {
    let client = Client::new_with_url(&URL).unwrap();
    let generation = client.generation().await.unwrap();
    let res = generation.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_growth_rate() {
    let client = Client::new_with_url(&URL).unwrap();
    let rate = client.growth_rate().await.unwrap();
    let res = rate.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_item() {
    let client = Client::new_with_url(&URL).unwrap();
    let item = client.item().await.unwrap();
    let res = item.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_item_attribute() {
    let client = Client::new_with_url(&URL).unwrap();
    let attribute = client.item_attribute().await.unwrap();
    let res = attribute.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_item_category() {
    let client = Client::new_with_url(&URL).unwrap();
    let category = client.item_category().await.unwrap();
    let res = category.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_item_fling_effect() {
    let client = Client::new_with_url(&URL).unwrap();
    let effect = client.item_fling_effect().await.unwrap();
    let res = effect.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_item_pocket() {
    let client = Client::new_with_url(&URL).unwrap();
    let pocket = client.item_pocket().await.unwrap();
    let res = pocket.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_language() {
    let client = Client::new_with_url(&URL).unwrap();
    let language = client.language().await.unwrap();
    let res = language.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_location() {
    let client = Client::new_with_url(&URL).unwrap();
    let location = client.location().await.unwrap();
    let res = location.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_location_area() {
    let client = Client::new_with_url(&URL).unwrap();
    let area = client.location_area().await.unwrap();
    let res = area.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_machine() {
    let client = Client::new_with_url(&URL).unwrap();
    let machine = client.machine().await.unwrap();
    let res = machine.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_move() {
    let client = Client::new_with_url(&URL).unwrap();
    let mov = client.r#move().await.unwrap();
    let res = mov.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_move_ailment() {
    let client = Client::new_with_url(&URL).unwrap();
    let ailment = client.move_ailment().await.unwrap();
    let res = ailment.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_move_battle_style() {
    let client = Client::new_with_url(&URL).unwrap();
    let style = client.move_battle_style().await.unwrap();
    let res = style.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_move_category() {
    let client = Client::new_with_url(&URL).unwrap();
    let category = client.move_category().await.unwrap();
    let res = category.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_move_damage_class() {
    let client = Client::new_with_url(&URL).unwrap();
    let class = client.move_damage_class().await.unwrap();
    let res = class.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_move_learn_method() {
    let client = Client::new_with_url(&URL).unwrap();
    let method = client.move_learn_method().await.unwrap();
    let res = method.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_move_target() {
    let client = Client::new_with_url(&URL).unwrap();
    let target = client.move_target().await.unwrap();
    let res = target.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_nature() {
    let client = Client::new_with_url(&URL).unwrap();
    let nature = client.nature().await.unwrap();
    let res = nature.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_pal_park_area() {
    let client = Client::new_with_url(&URL).unwrap();
    let area = client.pal_park_area().await.unwrap();
    let res = area.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_pokeathlon_stat() {
    let client = Client::new_with_url(&URL).unwrap();
    let stat = client.pokeathlon_stat().await.unwrap();
    let res = stat.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_pokedex() {
    let client = Client::new_with_url(&URL).unwrap();
    let pokedex = client.pokedex().await.unwrap();
    let res = pokedex.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_pokemon() {
    let client = Client::new_with_url(&URL).unwrap();
    let pokemon = client.pokemon().await.unwrap();
    let res = pokemon.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_pokemon_color() {
    let client = Client::new_with_url(&URL).unwrap();
    let color = client.pokemon_color().await.unwrap();
    let res = color.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_pokemon_form() {
    let client = Client::new_with_url(&URL).unwrap();
    let form = client.pokemon_form().await.unwrap();
    let res = form.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_pokemon_habitat() {
    let client = Client::new_with_url(&URL).unwrap();
    let habitat = client.pokemon_habitat().await.unwrap();
    let res = habitat.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_pokemon_shape() {
    let client = Client::new_with_url(&URL).unwrap();
    let shape = client.pokemon_shape().await.unwrap();
    let res = shape.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_pokemon_species() {
    let client = Client::new_with_url(&URL).unwrap();
    let species = client.pokemon_species().await.unwrap();
    let res = species.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_region() {
    let client = Client::new_with_url(&URL).unwrap();
    let region = client.region().await.unwrap();
    let res = region.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_stat() {
    let client = Client::new_with_url(&URL).unwrap();
    let stat = client.stat().await.unwrap();
    let res = stat.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_super_contest_effect() {
    let client = Client::new_with_url(&URL).unwrap();
    let effect = client.super_contest_effect().await.unwrap();
    let res = effect.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_type() {
    let client = Client::new_with_url(&URL).unwrap();
    let typ = client.r#type().await.unwrap();
    let res = typ.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_version() {
    let client = Client::new_with_url(&URL).unwrap();
    let version = client.version().await.unwrap();
    let res = version.id(1, &client).await;
    assert!(res.is_ok());
}

#[tokio::test]
async fn get_version_group() {
    let client = Client::new_with_url(&URL).unwrap();
    let group = client.version_group().await.unwrap();
    let res = group.id(1, &client).await;
    assert!(res.is_ok());
}
