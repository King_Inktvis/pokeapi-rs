use crate::config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn test_item() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.item().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_item_attributes() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.item_attribute().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_item_categories() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.item_category().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_item_fling_effects() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.item_fling_effect().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_item_pockets() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.item_pocket().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}
