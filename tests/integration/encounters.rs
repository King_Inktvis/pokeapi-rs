use crate::config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn test_encounter_condition_values() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.encounter_condition_value().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_encounter_condiction() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.encounter_condition().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_encounter_methods() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.encounter_method().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}
