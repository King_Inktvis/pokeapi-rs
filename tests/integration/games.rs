use crate::config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn test_generations() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.generation().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_pokedexes() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.pokedex().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_version() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.version().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_version_groups() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.version_group().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}
