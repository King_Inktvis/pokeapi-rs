use crate::config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn test_contest_effects() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.contest_effect().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_contest_types() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.contest_type().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_super_contest_effects() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.super_contest_effect().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}
