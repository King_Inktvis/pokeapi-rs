use lazy_static::lazy_static;
use std::env;

lazy_static! {
    pub static ref URL: String = {
        match env::var("URL") {
            Ok(url) => url,
            Err(_) => "https://pokeapi.co/".into(),
        }
    };
}
