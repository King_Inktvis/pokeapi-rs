use crate::config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn test_evolution_chains() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.evolution_chain().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_evolution_triggers() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.evolution_trigger().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}
