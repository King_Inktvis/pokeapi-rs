use crate::config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn test_berries() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.berry().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_berry_firmness() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.berry_firmness().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_berry_flavors() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.berry_flavor().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}
