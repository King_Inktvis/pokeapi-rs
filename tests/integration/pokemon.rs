use crate::config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn test_abilities() {
    let client = Client::new_with_url(&URL).unwrap();
    for entry in client.ability().await.unwrap().results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_characteristics() {
    let client = Client::new_with_url(&URL).unwrap();
    for entry in client.characteristic().await.unwrap().results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_egg_groups() {
    let client = Client::new_with_url(&URL).unwrap();
    for entry in client.egg_group().await.unwrap().results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_genders() {
    let client = Client::new_with_url(&URL).unwrap();
    for entry in client.gender().await.unwrap().results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_growth_rates() {
    let client = Client::new_with_url(&URL).unwrap();
    for entry in client.growth_rate().await.unwrap().results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_natures() {
    let client = Client::new_with_url(&URL).unwrap();
    for entry in client.nature().await.unwrap().results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_pokeathlon_stats() {
    let client = Client::new_with_url(&URL).unwrap();
    for entry in client.pokeathlon_stat().await.unwrap().results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_pokemon() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.pokemon().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_pokemon_colors() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.pokemon_color().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_pokemon_forms() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.pokemon_form().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_pokemon_habitats() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.pokemon_habitat().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_pokemon_shapes() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.pokemon_shape().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_pokemon_species() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.pokemon_species().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_stats() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.stat().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_types() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.r#type().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}
