use crate::config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn test_location_areas() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.location_area().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_locations() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.location().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_pal_park_areas() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.pal_park_area().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_regions() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.region().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}
