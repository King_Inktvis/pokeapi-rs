use crate::config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn test_machines() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.machine().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}
