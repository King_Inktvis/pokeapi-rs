use crate::config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn test_languages() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.language().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}
