use crate::config::URL;
use pokeapi_rs::Client;

#[tokio::test]
async fn test_move_ailments() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.move_ailment().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_move_battle_styles() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.move_battle_style().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_move_categories() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.move_category().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_move_damage_classes() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.move_damage_class().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_move_learn_methods() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.move_learn_method().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_move_targets() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.move_target().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}

#[tokio::test]
async fn test_moves() {
    let client = Client::new_with_url(&URL).unwrap();
    let list = client.r#move().await.unwrap();
    list.populate_cache(&client).await;
    for entry in list.results.iter() {
        entry.get_item(&client).await.expect(&entry.url);
    }
}
