use proc_macro::{self, TokenStream};
use quote::quote;
use syn::{parse_macro_input, DeriveInput, FieldsNamed};

#[proc_macro_derive(Getters)]
pub fn describe(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let name = input.ident;
    let s = if let syn::Data::Struct(s) = input.data {
        s
    } else {
        panic!()
    };
    if let syn::Fields::Named(FieldsNamed { named, .. }) = s.fields {
        let idents = named.iter().map(|f| {
            let returns = &f.ty;
            let field_name = &f.ident.clone().unwrap();
            let ty = if let syn::Type::Path(ty) = returns {
                ty
            } else {
                panic!()
            };
            match ty.path.segments[0].ident.to_string().as_str() {
                "i32" => {
                    quote!(
                        pub fn #field_name(&self) -> #returns {
                            self.#field_name
                        }
                    )
                }
                "u8" | "i8" | "u16" | "i16" => {
                    quote!(
                        pub fn #field_name(&self) -> i32 {
                            self.#field_name as i32
                        }
                    )
                }
                "Option" => {
                    let a = match &ty.path.segments[0].arguments {
                        syn::PathArguments::AngleBracketed(a) => a,
                        _ => panic!(),
                    };
                    let t = match &a.args[0] {
                        syn::GenericArgument::Type(t) => t,
                        _ => panic!(),
                    };
                    let p = match t {
                        syn::Type::Path(p) => p,
                        _ => panic!(),
                    };
                    let inner_type = &p.path.segments[0].ident;
                    match inner_type.to_string().as_str() {
                        "u8" | "i8" | "u16" | "i16" => {
                            quote!(
                                pub fn #field_name(&self) -> Option<i32> {
                                    match self.#field_name {
                                        Some(v)=>Some(v as i32),
                                        None=>None,
                                    }
                                }
                            )
                        }
                        "String" => {
                            quote!(
                                pub fn #field_name(&self) -> Option<&str> {
                                    match &self.#field_name {
                                        Some(v)=>Some(v.as_str()),
                                        None=>None,
                                    }
                                }
                            )
                        }
                        "NamedAPIResource" | "APIResource" => {
                            let a = match &p.path.segments[0].arguments {
                                syn::PathArguments::AngleBracketed(a) => a,
                                _ => panic!(),
                            };
                            let t = match &a.args[0] {
                                syn::GenericArgument::Type(t) => t,
                                _ => panic!(),
                            };
                            let p = match t {
                                syn::Type::Path(p) => p,
                                _ => panic!(),
                            };
                            let g_type = &p.path.segments[0].ident;

                            quote!(
                                pub fn #field_name(&self) -> Option<&#inner_type<#g_type>> {
                                    match &self.#field_name {
                                        Some(v)=> Some(v),
                                        None => None,
                                    }
                                }
                            )
                        }
                        _ => {
                            quote!(
                                pub fn #field_name(&self) -> &#returns {
                                    &self.#field_name
                                }
                            )
                        }
                    }
                }
                "String" => {
                    quote!(
                        pub fn #field_name(&self) -> &str {
                            self.#field_name.as_str()
                        }
                    )
                }
                _ => {
                    quote!(
                        pub fn #field_name(&self) -> &#returns {
                            &self.#field_name
                        }
                    )
                }
            }
        });
        let output = quote! {
            impl #name {
                #(#idents)*
            }
        };
        return output.into();
    };
    panic!()
}
